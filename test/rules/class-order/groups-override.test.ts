import generateRuleTests from "ember-template-lint/lib/helpers/rule-test-harness";
import plugin from "../../../src/index";

generateRuleTests({
  name: "class-order",

  groupMethodBefore: beforeEach,
  groupingMethod: describe,
  testMethod: it,
  plugins: [plugin],
  config: {
    groups: [
      {
        sortBy: "alphabet",
        matchBy: "all",
      },
    ],
  },

  good: [`<div class="bg-white md:flex p-1 rounded-lg"></div>`],

  bad: [
    {
      template: `<div class="md:flex bg-white rounded-lg p-2 lg:my-2"></div>`,
      fixedTemplate: `<div class="bg-white lg:my-2 md:flex p-2 rounded-lg"></div>`,
      result: {
        message:
          "HTML class attribute sorting is: 'md:flex bg-white rounded-lg p-2 lg:my-2', but should be: 'bg-white lg:my-2 md:flex p-2 rounded-lg'",
        line: 1,
        column: 5,
        isFixable: true,
        source: `<div class="md:flex bg-white rounded-lg p-2 lg:my-2"></div>`,
      },
    },
  ],
});
