export default function isUnfixable(classAttributeValue: string): boolean {
  return /(\S{{)|(}}\S)/.test(classAttributeValue.slice(1, -1));
}
