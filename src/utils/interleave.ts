import { AST } from "ember-template-recast";

export default function interleave(
  array: (AST.TextNode | AST.MustacheStatement)[],
  thing: AST.TextNode
): (AST.TextNode | AST.MustacheStatement)[] {
  return array.flatMap((value, index, array) =>
    array.length - 1 !== index // check for the last item
      ? [value, thing]
      : value
  );
}
