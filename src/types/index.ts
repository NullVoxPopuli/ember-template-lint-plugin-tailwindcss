import { AST } from "ember-template-recast";
export interface ClassOrderConfig {
  groups: Group[];
  matchers: Matchers;
  sorters: Sorters;
  linebreakBetweenGroups: LinebreakBetweenGroups;
  disableForMustaches: boolean;
  warnForConcat: boolean;
}

export type FullClassOrderConfig = ClassOrderConfig | boolean;

export interface ClassWrapConfig {
  classesPerLine: number | undefined;
}

export type FullClassWrapConfig = ClassWrapConfig | boolean;

export interface Loc {
  start: {
    column: number;
  };
  end: {
    column: number;
  };
}

export interface ReconstructArguments {
  groups: ClassesGroups;
  classAttribute: AST.AttrNode;
  onlyWithHint?: OnlyWithHint;
  indent: number;
}

export interface Group {
  matchBy: string;
  sortBy: string;
  order: number;
}

export type ClassesGroups = string[][];

export interface Matchers {
  [index: string]: Matcher;
}

export type Matcher = (item: string) => boolean;

export interface Sorters {
  [index: string]: Sorter;
}

export type Sorter = ((a: string, b: string) => number) | undefined;

export type OnlyWithHint = string | false;

export interface LinebreakBetweenGroupsConfig {
  onlyWithHint: OnlyWithHint;
  indent: number;
}

export type LinebreakBetweenGroups = LinebreakBetweenGroupsConfig | false;
