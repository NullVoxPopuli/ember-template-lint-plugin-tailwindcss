# class-wrap

The `extends: 'recommended'` property in a configuration file enables this rule.

Enforces `class` attribute wrapping after given `classesPerLine` classes.
This improves readibility and `--fix` for automatic class wrapping.

## Examples

Given that `classesPerLine=3`, this rule **forbids** the following:

```hbs
<div class="text-gray-900 leading-tight block mt-1 text-lg font-semibold hover:underline">foo</div>
```

This rule **allows** the following:

```hbs
<div class="hover:underline block mt-1 
            text-lg font-semibold leading-tight 
            text-gray-900">
  foo
</div>
```

## Configuration

* boolean - `true` to enable / `false` to disable
* object -- An object with the following keys:
  * `classesPerLine` -- integer|null: Put a line-wrap in the class attribute after N classes